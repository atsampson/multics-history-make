# Utilities for multics-history-make tools.
# Copyright 2017, 2021 Adam Sampson <ats@offog.org>
#
# This program is released under the same license as Multics itself; see
# COPYING for further details.

import re
import sys

def die(*s):
    """Print an error message and exit."""
    sys.stdout.flush()
    print >>sys.stderr, "".join(map(str, s))
    sys.exit(1)

def md_filename(fn):
    """Given a filename, return the corresponding .md filename."""
    m = re.match(r'^(.*/)?([^/]+)$', fn)
    dn = m.group(1)
    return (dn if dn is not None else "") + "." + m.group(2) + ".md"

if __name__ == "__main__":
    assert md_filename("foo") == ".foo.md"
    assert md_filename("bar/foo") == "bar/.foo.md"
    assert md_filename("/quux/bar/foo") == "/quux/bar/.foo.md"
